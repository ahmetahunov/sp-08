package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.client.RestApiClient;
import ru.ahmetahunov.tm.dto.UserDTO;
import java.security.Principal;

@Controller
public class UserController {

	@Setter
	@NotNull
	@Autowired
	private RestApiClient restApiClient;

	@Setter
	@NotNull
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping("/registration")
	public String registration() {
		return "user/registration";
	}

	@PostMapping("/registration")
	public String registration(@NotNull final UserDTO userDTO) {
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		@Nullable UserDTO user = restApiClient.getUserByLogin(userDTO.getLogin());
		if (user != null) return "/user_exists";
		restApiClient.createUser(userDTO);
		return "redirect:/login";
	}

	@GetMapping("/admin/user_list")
	public String userList(@NotNull final Model model) {
		model.addAttribute("users", restApiClient.getUsers());
		return "user/admin_user_list";
	}

	@GetMapping("/admin/user_edit/{id}")
	public String userEditAdmin(
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("user", restApiClient.getUser(id));
		return "user/admin_user_edit";
	}

	@PostMapping("/admin/user_edit")
	public String userEditAdmin(@NotNull final UserDTO userDTO) {
		restApiClient.editUserAdmin(userDTO);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/user_edit/{id}")
	public String userEdit(
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("user", restApiClient.getUser(id));
		return "user/user_edit";
	}

	@PostMapping("/user/user_edit")
	public String userEdit(
			@NotNull final UserDTO userDTO,
			@NotNull final Principal principal
	) {
		@Nullable final UserDTO user = restApiClient.getUserByLogin(principal.getName());
		if (user == null) return "/error_page";
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		if (!passwordEncoder.matches(userDTO.getPassword(), user.getPassword())) return "/error_page";
		restApiClient.editUser(principal.getName(), userDTO);
		return "redirect:/";
	}

	@GetMapping("/admin/registration")
	public String registrationAdmin() {
		return "user/admin_registration";
	}

	@PostMapping("/admin/registration")
	public String registrationAdmin(@NotNull final UserDTO userDTO) {
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		@Nullable UserDTO user = restApiClient.getUserByLogin(userDTO.getLogin());
		if (user != null) return "/user_exists";
		restApiClient.createUserAdmin(userDTO);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/delete")
	public String removeUser(@NotNull final Principal principal) {
		restApiClient.removeUser(principal.getName());
		return "redirect:/logout";
	}

	@GetMapping("/admin/delete/{id}")
	public String removeUser(@PathVariable("id") @NotNull final String id) {
		restApiClient.removeUserAdmin(id);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/user_info")
	public String getInfo(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		model.addAttribute("user", restApiClient.getUserByLogin(principal.getName()));
		return "user/user_info";
	}

}
