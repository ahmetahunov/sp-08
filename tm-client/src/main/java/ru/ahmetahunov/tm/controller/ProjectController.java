package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.client.RestApiClient;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import java.security.Principal;

@Controller
public class ProjectController {

	@Setter
	@NotNull
	@Autowired
	private RestApiClient projectClient;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public String errorHandle() {
		return "exception";
	}

	@GetMapping("/project_list")
	public String projectList(@NotNull final Principal principal, @NotNull final Model model) {
		model.addAttribute("projects", projectClient.getProjects(principal.getName()));
		return "project/project_list";
	}

	@GetMapping("/project_info/{id}")
	public String projectInfo(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("project", projectClient.getProject(principal.getName(), id));
		return "project/project_info";
	}

	@GetMapping("/project_create")
	public String projectCreate() {
		return "project/project_create";
	}

	@PostMapping("/project_create")
	public String projectCreate(
			@NotNull final Principal principal,
			@NotNull final ProjectDTO projectDTO
	) {
		projectClient.addProject(principal.getName(), projectDTO);
		return "redirect:/project_list";
	}

	@GetMapping("/project_update/{id}")
	public String projectUpdate(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("projectEdit", projectClient.getProject(principal.getName(), id));
		return "project/project_update";
	}

	@PostMapping("/project_update")
	public String projectUpdate(
			@NotNull final Principal principal,
			@NotNull final ProjectDTO projectDTO,
			@NotNull final Model model
	) {
		model.addAttribute("project", projectClient.updateProject(principal.getName(), projectDTO));
		return "project/project_info";
	}

	@GetMapping("/project_delete/{id}")
	public String projectDelete(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id
	) {
		projectClient.removeProject(principal.getName(), id);
		return "redirect:/project_list";
	}

}
