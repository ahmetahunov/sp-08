package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.client.RestApiClient;
import ru.ahmetahunov.tm.dto.TaskDTO;
import java.security.Principal;
import java.util.List;

@Controller
public class TaskController {

	@Setter
	@NotNull
	@Autowired
	private RestApiClient taskClient;

	@Setter
	@NotNull
	@Autowired
	private RestApiClient projectClient;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public String errorHandle() {
		return "exception";
	}

	@GetMapping("/task_list")
	public String taskList(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		model.addAttribute("tasks", taskClient.getTasks(principal.getName()));
		return "task/task_list";
	}

	@GetMapping("/task_list/{projectId}")
	public String taskListByProjectId(
			@NotNull final Principal principal,
			@PathVariable("projectId") @NotNull final String id,
			@NotNull final Model model
	) {
		@NotNull final List<TaskDTO> tasks = taskClient.getTasksByProjectId(principal.getName(), id);
		model.addAttribute("tasks", tasks);
		model.addAttribute("project", projectClient.getProject(principal.getName(), id));
		return "task/task_list_by_project";
	}

	@GetMapping("/task_info/{id}")
	public String taskInfo(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("task", taskClient.getTask(principal.getName(), id));
		return "task/task_info";
	}

	@GetMapping("/task_create")
	public String taskCreate(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		model.addAttribute("projects", projectClient.getProjects(principal.getName()));
		return "task/task_create";
	}

	@PostMapping("/task_create")
	public String taskCreate(
			@NotNull final Principal principal,
			@NotNull final TaskDTO taskDTO,
			@NotNull final String projectId
	) {
		taskDTO.setProjectId(projectId);
		taskClient.addTask(principal.getName(), taskDTO);
		return "redirect:/task_list";
	}

	@GetMapping("/task_update/{id}")
	public String taskUpdate(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		@Nullable final TaskDTO task = taskClient.getTask(principal.getName(), id);
		model.addAttribute("taskEdit", task);
		model.addAttribute("projectEdit", projectClient.getProject(principal.getName(), task.getProjectId()));
		model.addAttribute("projects", projectClient.getProjects(principal.getName()));
		return "task/task_update";
	}

	@PostMapping("/task_update")
	public String taskUpdate(
			@NotNull final Principal principal,
			@NotNull final TaskDTO taskDTO,
			@NotNull final String projectId,
			@NotNull final Model model
	) {
		taskDTO.setProjectId(projectId);
		model.addAttribute("task", taskClient.updateTask(principal.getName(), taskDTO));
		return "task/task_info";
	}

	@GetMapping("/task_delete/{id}")
	public String taskDelete(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id
	) {
		taskClient.removeTask(principal.getName(), id);
		return "redirect:/task_list";
	}
	
}
