package ru.ahmetahunov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import ru.ahmetahunov.tm.dto.TaskDTO;
import ru.ahmetahunov.tm.dto.UserDTO;
import java.security.Principal;
import java.util.List;

@FeignClient("tm-server")
public interface RestApiClient {

	@GetMapping(value = "/users")
	public List<UserDTO> getUsers();

	@GetMapping(value = "/users/{id}")
	public UserDTO getUser(@PathVariable("id") @NotNull final String id);

	@PostMapping(value = "/users")
	public UserDTO createUser(@RequestBody @Nullable final UserDTO userDTO);

	@PostMapping(value = "/users/admin")
	public UserDTO createUserAdmin(@RequestBody @Nullable final UserDTO userDTO);

	@PutMapping(value = "/users")
	public UserDTO editUser(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PutMapping(value = "/users/admin")
	public UserDTO editUserAdmin(@RequestBody @Nullable final UserDTO userDTO);

	@DeleteMapping("/users")
	public void removeUser(@NotNull final String login);

	@DeleteMapping("/users/{id}")
	public void removeUserAdmin(@PathVariable("id") @NotNull final String id);

	@GetMapping("/users/user/{userName}")
	public UserDTO getUserByLogin(@PathVariable("userName") @NotNull final String userName);

	@GetMapping(value = "/projects")
	public List<ProjectDTO> getProjects(@RequestParam @NotNull final String login);

	@GetMapping(value = "/projects/{id}")
	public ProjectDTO getProject(
			@RequestParam @NotNull final String login,
			@PathVariable("id") String id
	);

	@PostMapping(value = "/projects")
	public ProjectDTO addProject(
			@RequestParam @NotNull final String login,
			@RequestBody ProjectDTO projectDTO
	);

	@PutMapping(value = "/projects")
	public ProjectDTO updateProject(
			@RequestParam @NotNull final String login,
			@RequestBody ProjectDTO projectDTO
	);

	@DeleteMapping("/projects/{id}")
	public void removeProject(
			@RequestParam @NotNull final String login,
			@PathVariable("id") String id
	);

	@GetMapping(value = "/tasks")
	public List<TaskDTO> getTasks(@RequestParam @NotNull final String login);

	@GetMapping(value = "/tasks/project/{projectId}")
	public List<TaskDTO> getTasksByProjectId(
			@RequestParam @NotNull final String login,
			@PathVariable("projectId") @NotNull final String projectId
	);

	@GetMapping(value = "/tasks/{id}")
	public TaskDTO getTask(
			@RequestParam @NotNull final String login,
			@PathVariable("id") @NotNull final String id
	);

	@PostMapping(value = "/tasks")
	public TaskDTO addTask(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@PutMapping(value = "/tasks")
	public TaskDTO updateTask(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@DeleteMapping("/tasks/{id}")
	public void removeTask(
			@RequestParam @NotNull final String login,
			@PathVariable("id") @NotNull final String id
	);

}
