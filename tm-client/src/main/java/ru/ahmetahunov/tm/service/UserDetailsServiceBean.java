package ru.ahmetahunov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.client.RestApiClient;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.enumerated.RoleType;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

	@Setter
	@Autowired
	private RestApiClient restApiClient;


	@Override
	@Transactional
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		@Nullable final UserDTO user = findUserByLogin(userName);
		if (user == null) throw new UsernameNotFoundException("User not found.");
		org.springframework.security.core.userdetails.User.UserBuilder builder = null;
		builder = org.springframework.security.core.userdetails.User.withUsername(userName);
		builder.password(user.getPassword());
		final List<RoleType> userRoles = user.getRoles();
		@NotNull final List<String> roles = new ArrayList<>();
		for (RoleType role : userRoles) roles.add(role.toString());
		builder.roles(roles.toArray(new String[] {}));
		return builder.build();
	}

	@Nullable
	private UserDTO findUserByLogin(@Nullable final String login) {
		if (login == null || login.isEmpty()) return null;
		return restApiClient.getUserByLogin(login);
	}

}
