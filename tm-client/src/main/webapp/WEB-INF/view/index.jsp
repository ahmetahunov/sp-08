<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Manager</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <div class="greeting">
        <h1>Task Manager welcome page</h1>
    </div>
    <jsp:include page="footer.jsp"/>
</body>
</html>
