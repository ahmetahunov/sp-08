<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <div class="center">
        <div class="login">
            <form name="user" action="${pageContext.request.contextPath}/login" method="post">
                <div>
                    <label for="username">Login:</label>
                    <input name="username" id="username" type="text" placeholder="Please enter login" required/>
                </div>
                <div>
                    <label for="password">Password:</label>
                    <input name="password" id="password" type="password" placeholder="Please enter password" required/>
                </div>
                <div>
                    <input type="submit" value="LOG-IN"/>
                </div>
            </form>
        </div>
    </div>
    <jsp:include page="footer.jsp"/>
</body>
</html>
