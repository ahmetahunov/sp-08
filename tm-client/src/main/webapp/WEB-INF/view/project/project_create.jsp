<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project create</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
    <body>
        <jsp:include page="../header.jsp"/>
        <jsp:useBean id="now" class="java.util.Date"/>
        <fmt:formatDate value="${now}" var="defaultDate" pattern="yyyy-MM-dd"/>
        <div class="center">
        <div class="table">
            <form name="projectDTO" action="${pageContext.request.contextPath}/project_create" method="post">
                <table class="info">
                    <caption>PROJECT CREATE</caption>
                    <tr>
                        <td>Name:</td>
                        <td><input type="text" id="name" name="name" required/></td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td><input type="text" id="description" name="description"/></td>
                    </tr>
                    <tr>
                        <td>Start date:</td>
                        <td><input path="startDate" type="date" id="startDate" name="startDate" value="${defaultDate}" required/></td>
                    </tr>
                    <tr>
                        <td>Finish date:</td>
                        <td><input type="date" id="finishDate" name="finishDate" value="${defaultDate}" required/></td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>
                            <select name="status" required>
                                <option value="PLANNED">PLANNED</option>
                                <option value="IN_PROGRESS">IN-PROGRESS</option>
                                <option value="DONE">DONE</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="SAVE" class="button green">
            </form>
        </div>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
