<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Project info</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div class="center">
        <div class="table">
            <table class="info">
                <caption>PROJECT INFO</caption>
                <tr>
                    <td>ID:</td>
                    <td>${project.id}</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>${project.name}</td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td>${project.description}</td>
                </tr>
                <tr>
                    <td>Creation date:</td>
                    <td><fmt:formatDate value="${project.creationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
                </tr>
                <tr>
                    <td>Start date:</td>
                    <td>
                        <c:if test="${project.startDate.time != 0}">
                            <fmt:formatDate value="${project.startDate}" pattern="dd-MM-yyyy" />
                        </c:if>
                        <c:if test="${project.startDate.time == 0}">
                            <p>NOT SET</p>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>Finish date:</td>
                    <td>
                        <c:if test="${project.finishDate.time != 0}">
                            <fmt:formatDate value="${project.finishDate}" pattern="dd-MM-yyyy" />
                        </c:if>
                        <c:if test="${project.finishDate.time == 0}">
                            <p>NOT SET</p>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td>${project.status}</td>
                </tr>
            </table>
        </div>
        <div class="button">
            <a href="${pageContext.request.contextPath}/project_update/${project.id}">
                <button type="button" class="green">EDIT</button>
            </a>
            <a href="${pageContext.request.contextPath}/project_delete/${project.id}">
                <button type="button" class="red">REMOVE</button>
            </a>
            <a href="${pageContext.request.contextPath}/project_list">
                <button type="button">PROJECT LIST</button>
            </a>
        </div>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
