<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project edit</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div class="center">
        <div class="table">
            <form name="projectDTO" action="${pageContext.request.contextPath}/project_update" method="post">
                <table class="info">
                    <caption>PROJECT EDIT</caption>
                    <tr>
                        <td>ID:</td>
                        <td><input name="id" value="${projectEdit.id}" readonly></td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td><input type="text" id="name" name="name" value="${projectEdit.name}" required></td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td><input type="text" id="description" name="description" value="${projectEdit.description}"></td>
                    </tr>
                    <tr>
                        <td>Start date:</td>
                        <fmt:formatDate type="date" value="${projectEdit.startDate}" var="sDate" pattern="yyyy-MM-dd"/>
                        <td><input type="date" id="startDate" name="startDate" value="${sDate}" required></td>
                    </tr>
                    <tr>
                        <td>Finish date:</td>
                        <fmt:formatDate type="date" value="${projectEdit.finishDate}" var="fDate" pattern="yyyy-MM-dd"/>
                        <td><input type="date" id="finishDate" name="finishDate" value="${fDate}" required></td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>
                            <select name="status" required>
                                <option value="${projectEdit.status}" disabled>${projectEdit.status}</option>
                                <option value="PLANNED">PLANNED</option>
                                <option value="IN_PROGRESS">IN-PROGRESS</option>
                                <option value="DONE">DONE</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="UPDATE" class="button green">
            </form>
        </div>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
