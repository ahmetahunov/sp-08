<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project list</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div class="center">
        <div class="table">
            <c:if test="${projects.size() != 0}">
                <table class="main">
                    <caption>PROJECT MANAGEMENT</caption>
                    <tr>
                        <th>№</th>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>TASKS</th>
                        <th>VIEW</th>
                        <th>EDIT</th>
                        <th>REMOVE</th>
                    </tr>
                    <c:forEach items="${projects}" var="project" varStatus="status">
                        <tr>
                            <td>${status.count}</td>
                            <td>${project.id}</td>
                            <td>${project.name}</td>
                            <td>${project.description}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/task_list/${project.id}">
                                    <img src="<c:url value=" ${pageContext.request.contextPath}/icon/list.png"/>" alt="list"/>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.request.contextPath}/project_info/${project.id}">
                                    <img src="<c:url value=" ${pageContext.request.contextPath}/icon/info.png"/>" alt="info"/>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.request.contextPath}/project_update/${project.id}">
                                    <img src="<c:url value=" ${pageContext.request.contextPath}/icon/pencil.png"/>" alt="edit"/>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.request.contextPath}/project_delete/${project.id}">
                                    <img src="<c:url value=" ${pageContext.request.contextPath}/icon/garbage.png"/>" alt="remove"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <c:if test="${projects.size() == 0}">
                <p>Project list is empty</p>
            </c:if>
        </div>
        <div class="button">
            <a href="${pageContext.request.contextPath}/project_create">
                <button type="button" class="green">CREATE NEW PROJECT</button>
            </a>
        </div>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
