<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User edit admin</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
<jsp:include page="../header.jsp"/>
<div class="center">
    <div class="table">
        <form name="userDTO" action="${pageContext.request.contextPath}/admin/user_edit" method="post">
            <table class="info">
                <caption>USER EDIT</caption>
                <tr>
                    <td>ID:</td>
                    <td><input name="id" value="${user.id}" readonly></td>
                </tr>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" id="login" name="login" value="${user.login}" readonly></td>
                </tr>
                <tr>
                    <td>
                        <label for="user-role">USER</label>
                        <input id="user-role" type="checkbox" name="roles" value="USER" checked required>
                    </td>
                    <td>
                        <label for="admin-role">ADMINISTRATOR</label>
                        <input id="admin-role" type="checkbox" name="roles" value="ADMINISTRATOR">
                    </td>
                </tr>
                <tr>
                    <td>New password:</td>
                    <td><input type="password" id="passwordNew" name="passwordNew"></td>
                </tr>
            </table>
            <input type="submit" value="UPDATE" class="button green">
        </form>
    </div>
</div>
<jsp:include page="../footer.jsp"/>
</body>
</html>