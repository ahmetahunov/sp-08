<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Info</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div class="center">
            <div class="table">
                <table class="info">
                    <caption>USER INFO</caption>
                    <tr>
                        <td>ID:</td>
                        <td>${user.id}</td>
                    </tr>
                    <tr>
                        <td>Login:</td>
                        <td>${user.login}</td>
                    </tr>
                </table>
            </div>
            <div class="button">
                <a href="${pageContext.request.contextPath}/user/user_edit/${user.id}">
                    <button type="button" class="green">EDIT</button>
                </a>
                <a href="${pageContext.request.contextPath}/user/delete/">
                    <button type="button" class="red">REMOVE</button>
                </a>
            </div>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
