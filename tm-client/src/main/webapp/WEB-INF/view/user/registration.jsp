<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="../header.jsp"/>
    <div class="center">
        <div class="table">
            <form name="userDTO" action="${pageContext.request.contextPath}/registration" method="post">
                <table class="info">
                    <caption>USER REGISTRATION</caption>
                    <tr>
                        <td>Login:</td>
                        <td><input type="text" id="login" name="login" required></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" id="password" name="password" required></td>
                    </tr>
                    <tr>
                        <td>Password check:</td>
                        <td><input type="password" id="passwordCheck" name="passwordCheck" required></td>
                    </tr>
                </table>
                <input type="submit" value="REGISTER" class="button green">
            </form>
        </div>
    </div>
    <jsp:include page="../footer.jsp"/>
</body>
</html>
