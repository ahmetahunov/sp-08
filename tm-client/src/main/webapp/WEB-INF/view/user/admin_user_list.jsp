<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User List</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
<jsp:include page="../header.jsp"/>
<div class="center">
    <div class="table">
        <c:if test="${users.size() != 0}">
            <table class="main">
                <caption>USER MANAGEMENT</caption>
                <tr>
                    <th>№</th>
                    <th>Id</th>
                    <th>Login</th>
                    <th>EDIT</th>
                    <th>REMOVE</th>
                </tr>
                <c:forEach items="${users}" var="user" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${user.id}</td>
                        <td>${user.login}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/admin/user_edit/${user.id}">
                                <img src="<c:url value=" ${pageContext.request.contextPath}/icon/pencil.png"/>" alt="edit"/>
                            </a>
                        </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/admin/delete/${user.id}">
                                <img src="<c:url value=" ${pageContext.request.contextPath}/icon/garbage.png"/>" alt="remove"/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <c:if test="${users.size() == 0}">
            <p>User list is empty</p>
        </c:if>
    </div>
    <div class="button">
        <a href="${pageContext.request.contextPath}/admin/registration">
            <button type="button" class="green">CREATE NEW USER</button>
        </a>
    </div>
</div>
<jsp:include page="../footer.jsp"/>
</html>
