<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task list</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="../header.jsp"/>
    <div class="center">
    <div class="table">
        <table class="helper">
            <caption>PROJECT</caption>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
            <tr>
                <td>${project.id}</td>
                <td>${project.name}</td>
                <td>${project.description}</td>
            </tr>
        </table>
        <c:if test="${tasks.size() != 0}">
            <table>
                <caption>TASK LIST</caption>
                <tr>
                    <th>№</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>INFO</th>
                    <th>EDIT</th>
                    <th>REMOVE</th>
                </tr>
                <c:forEach items="${tasks}" var="task" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${task.id}</td>
                        <td>${task.name}</td>
                        <td>${task.description}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/task_info/${task.id}">
                                <img src="<c:url value=" ${pageContext.request.contextPath}/icon/info.png"/>" alt="info"/>
                            </a>
                        </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/task_update/${task.id}">
                                <img src="<c:url value=" ${pageContext.request.contextPath}/icon/pencil.png"/>" alt="edit"/>
                            </a>
                        </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/task_delete/${task.id}">
                                <img src="<c:url value=" ${pageContext.request.contextPath}/icon/garbage.png"/>" alt="remove"/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <c:if test="${tasks.size() == 0}">
            <p>Task list is empty</p>
        </c:if>
    </div>
    <div class="button">
        <a href="${pageContext.request.contextPath}/task_create">
            <button type="button" class="green">CREATE NEW TASK</button>
        </a>
    </div>
    </div>
    <jsp:include page="../footer.jsp"/>
</body>
</html>
