<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task update</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="../header.jsp"/>
    <div class="center">
    <div class="table">
        <form name="taskDTO" action="${pageContext.request.contextPath}/task_update" method="post">
            <table class="info">
                <caption>TASK EDIT</caption>
                <tr>
                    <td>Project:</td>
                    <td>
                        <select name="projectId">
                            <c:forEach items="${projects}" var="project">
                                <option value="${project.id}">${project.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>ID:</td>
                    <td><input name="id" value="${taskEdit.id}" readonly></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" id="name" name="name" value="${taskEdit.name}" required></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><input type="text" id="description" name="description" value="${taskEdit.description}"></td>
                </tr>
                <tr>
                    <td>Start date:</td>
                        <fmt:formatDate type="date" value="${taskEdit.startDate}" var="sDate" pattern="yyyy-MM-dd"/>
                    <td><input type="date" id="startDate" name="startDate" value="${sDate}" required></td>
                </tr>
                <tr>
                    <td>Finish date:</td>
                        <fmt:formatDate type="date" value="${taskEdit.finishDate}" var="fDate" pattern="yyyy-MM-dd"/>
                    <td><input type="date" id="finishDate" name="finishDate" value="${fDate}" required></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td>
                        <select name="status" required>
                            <option value="${taskEdit.status}" disabled>${taskEdit.status}</option>
                            <option value="PLANNED">PLANNED</option>
                            <option value="IN_PROGRESS">IN-PROGRESS</option>
                            <option value="DONE">DONE</option>
                        </select>
                    </td>
                </tr>
            </table>
            <input type="submit" value="UPDATE" class="button green">
        </form>
    </div>
    </div>
    <jsp:include page="../footer.jsp"/>
</body>
</html>
