<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task info</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="../header.jsp"/>
    <div class="center">
    <div class="table">
        <table class="info">
            <caption>TASK INFO</caption>
            <tr>
                <td>ID:</td>
                <td>${task.id}</td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>${task.name}</td>
            </tr>
            <tr>
                <td>Description:</td>
                <td>${task.description}</td>
            </tr>
            <tr>
                <td>Creation date:</td>
                <td><fmt:formatDate value="${task.creationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
            </tr>
            <tr>
                <td>Start date:</td>
                <td>
                    <c:if test="${task.startDate.time != 0}">
                        <fmt:formatDate value="${task.startDate}" pattern="dd-MM-yyyy" />
                    </c:if>
                    <c:if test="${task.startDate.time == 0}">
                        <p>NOT SET</p>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>Finish date:</td>
                <td>
                    <c:if test="${task.finishDate.time != 0}">
                        <fmt:formatDate value="${task.finishDate}" pattern="dd-MM-yyyy" />
                    </c:if>
                    <c:if test="${task.finishDate.time == 0}">
                        <p>NOT SET</p>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>Status:</td>
                <td>${task.status}</td>
            </tr>
            <tr>
                <td>Project ID:</td>
                <td>${task.projectId}</td>
            </tr>
        </table>
    </div>
    <div class="button">
        <a href="${pageContext.request.contextPath}/task_update/${task.id}">
            <button type="button" class="green">EDIT</button>
        </a>
        <a href="${pageContext.request.contextPath}/task_delete/${task.id}">
            <button type="button" class="red">REMOVE</button>
        </a>
        <a href="${pageContext.request.contextPath}/task_list">
            <button type="button">TASK LIST</button>
        </a>
    </div>
    </div>
    <jsp:include page="../footer.jsp"/>
</body>
</html>
