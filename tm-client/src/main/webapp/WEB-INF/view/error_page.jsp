<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Error</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="picture">
    <p>Something wrong! Check entered information and try again!</p>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>