https://gitlab.com/ahmetahunov/sp-08
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven
+ PostgreSQL
+ Spring-boot

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/sp-08.git
cd sp-08
mvn clean install
```

## run discovery-service
```bash
cd eureka-discovery-service
mvn spring-boot:run
```

## run config-server
```bash
cd config-server
mvn spring-boot:run
```

## run zuul-proxy-gateway
```bash
cd zuul-proxy-gateway
mvn spring-boot:run
```

## run tm-server
```bash
cd tm-server
mvn spring-boot:run
```

## run tm-client
```bash
cd tm-client
mvn spring-boot:run
```
