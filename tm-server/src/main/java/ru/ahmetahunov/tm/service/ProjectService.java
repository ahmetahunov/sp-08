package ru.ahmetahunov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.entity.Project;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Setter
    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        if (project.getName().isEmpty()) return null;
        return repository.save(project);
    }

    @Override
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        if (project.getName().isEmpty()) return null;
        return repository.save(project);
    }

    @Nullable
    @Override
    public Project findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return repository.findByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        repository.deleteByUserIdAndId(userId, id);
    }

}
