package ru.ahmetahunov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Task;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Setter
    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        if (task.getName().isEmpty()) return null;
        return repository.save(task);
    }

    @Override
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        if (task.getName().isEmpty()) return null;
        return repository.save(task);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return repository.findByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllTasksByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    public void remove(String userId, String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        repository.deleteByUserIdAndId(userId, id);
    }

}
