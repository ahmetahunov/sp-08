package ru.ahmetahunov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.repository.UserRepository;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

	@Setter
	@Autowired
	private UserRepository userRepository;

	@Nullable
	@Override
	public User persist(@Nullable final User user) {
		if (user == null) return null;
		if (user.getLogin() == null || user.getLogin().isEmpty()) return null;
		if (userRepository.findUserByLogin(user.getLogin()) != null) return null;
		return userRepository.save(user);
	}

	@Nullable
	@Override
	public User merge(@Nullable final User user) {
		if (user == null) return null;
		if (user.getLogin() == null || user.getLogin().isEmpty()) return null;
		return userRepository.save(user);
	}

	@Nullable
	@Override
	public User findOne(@Nullable final String id) {
		if (id == null || id.isEmpty()) return null;
		return userRepository.findById(id).orElse(null);
	}

	@NotNull
	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Nullable
	@Override
	public User findByLogin(@Nullable final String login) {
		if (login == null || login.isEmpty()) return null;
		return userRepository.findUserByLogin(login);
	}


	@Override
	public void remove(@Nullable final String id) {
		if (id == null || id.isEmpty()) return;
		userRepository.deleteById(id);
	}

}
