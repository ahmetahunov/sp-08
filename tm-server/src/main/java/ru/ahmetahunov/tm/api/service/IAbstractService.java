package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    public T persist(T item);

    public T merge(T item);

    @Nullable
    public T findOne(String id);

    @NotNull
    public List<T> findAll();

    public void remove(String id);

}
