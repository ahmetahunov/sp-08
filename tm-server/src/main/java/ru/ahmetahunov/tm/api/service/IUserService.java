package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;

public interface IUserService extends IAbstractService<User> {

	@Nullable
	public User findByLogin(String login);

}
