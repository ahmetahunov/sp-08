package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    @Nullable
    public Task findOne(String userId, String id);

    @NotNull
    public List<Task> findAll(String userId);

    @NotNull
    public List<Task> findAll(String userId, String projectId);

    public void remove(String userId, String id);

}
