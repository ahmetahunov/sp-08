package ru.ahmetahunov.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.enumerated.Status;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date startDate = new Date(0);

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date finishDate = new Date(0);

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private String projectId = "";

    @NotNull
    private String userId = "";

    @NotNull
    public Task transformToTask(
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService
            ) {
        @NotNull final Task task = new Task();
        task.setId(this.id);
        task.setName(this.name);
        task.setDescription(this.description);
        task.setStartDate(this.startDate);
        task.setFinishDate(this.finishDate);
        task.setCreationDate(this.creationDate);
        task.setStatus(this.status);
        task.setProject(projectService.findOne(this.projectId));
        task.setUser(userService.findOne(this.userId));
        return task;
    }

}
