package ru.ahmetahunov.tm.restcontroller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.TaskDTO;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskRestController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@GetMapping(
			value = "/tasks/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public TaskDTO getTask(
			@RequestParam @NotNull final String login,
			@PathVariable("id")@NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		@Nullable final Task task = taskService.findOne(user.getId(), id);
		return (task == null) ? null : task.transformToDTO();
	}

	@GetMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<TaskDTO> getTasks(@RequestParam @NotNull final String login) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return Collections.emptyList();
		return taskService.findAll(user.getId())
				.stream()
				.map(Task::transformToDTO)
				.collect(Collectors.toList());
	}

	@GetMapping(
			value = "/tasks/project/{projectId}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<TaskDTO> getTasksByProject(
			@RequestParam @NotNull final String login,
			@PathVariable("projectId") @NotNull final String projectId
	) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return Collections.emptyList();
		return taskService
				.findAll(user.getId(), projectId)
				.stream()
				.map(Task::transformToDTO)
				.collect(Collectors.toList());
	}

	@PostMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public TaskDTO addTask(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final TaskDTO taskDTO
	) {
		if (taskDTO == null) return null;
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		taskDTO.setUserId(user.getId());
		return taskService.persist(taskDTO.transformToTask(projectService, userService)).transformToDTO();
	}

	@PutMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public TaskDTO updateTask(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final TaskDTO taskDTO
	) {
		if (taskDTO == null) return null;
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		taskDTO.setUserId(user.getId());
		return taskService.merge(taskDTO.transformToTask(projectService, userService)).transformToDTO();
	}

	@DeleteMapping("/tasks/{id}")
	public void removeTask(
			@RequestParam @NotNull final String login,
			@PathVariable("id") @NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return;
		@Nullable final Task task = taskService.findOne(user.getId(), id);
		if (task == null) return;
		taskService.remove(task.getId());
	}

}
