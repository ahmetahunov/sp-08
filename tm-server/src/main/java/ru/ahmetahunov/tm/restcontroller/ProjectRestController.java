package ru.ahmetahunov.tm.restcontroller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectRestController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@GetMapping(
			value = "/projects/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO getProject(
			@RequestParam @NotNull final String login,
			@PathVariable("id")@NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		@Nullable final Project project = projectService.findOne(user.getId(), id);
		return (project == null) ? null : project.transformToDTO();
	}

	@GetMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<ProjectDTO> getProjects(@RequestParam @NotNull final String login) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return Collections.emptyList();
		return projectService.findAll(user.getId())
				.stream()
				.map(Project::transformToDTO)
				.collect(Collectors.toList());
	}

	@PostMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO addProject(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final ProjectDTO projectDTO
	) {
		if (projectDTO == null) return null;
		@Nullable User user = userService.findByLogin(login);
		if (user == null) return null;
		projectDTO.setUserId(user.getId());
		@NotNull final Project created = projectService.persist(projectDTO.transformToEntity(taskService, userService));
		return (created == null) ? null : created.transformToDTO();
	}

	@PutMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO updateProject(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final ProjectDTO projectDTO
	) {
		if (projectDTO == null) return null;
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		projectDTO.setUserId(user.getId());
		@Nullable final Project project = projectService.merge(projectDTO.transformToEntity(taskService, userService));
		return (project == null) ? null : project.transformToDTO();
	}

	@DeleteMapping("/projects/{id}")
	public void removeProject(
			@RequestParam @NotNull final String login,
			@PathVariable("id") @NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return;
		@Nullable final Project project = projectService.findOne(user.getId(), id);
		if (project == null) return;
		projectService.remove(project.getId());
	}

}
