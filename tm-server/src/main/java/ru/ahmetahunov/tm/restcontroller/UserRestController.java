package ru.ahmetahunov.tm.restcontroller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.entity.Role;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.RoleType;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserRestController {

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@NotNull
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	@GetMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<UserDTO> getUsers() {
		return userService.findAll().stream().map(User::transformToDTO).collect(Collectors.toList());
	}

	@GetMapping(
			value = "/users/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO getUser(@PathVariable("id") @NotNull final String id) {
		@Nullable final User user = userService.findOne(id);
		return (user == null) ? null : user.transformToDTO();
	}

	@PostMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO createUser(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@NotNull final User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		@NotNull final Role role = new Role();
		role.setUser(user);
		user.getRoles().add(role);
		return userService.persist(user).transformToDTO();
	}

	@PostMapping(
			value = "/users/admin",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO createUserAdmin(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@NotNull final User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		return userService.persist(user).transformToDTO();
	}

	@PutMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO editUser(
			@RequestParam @NotNull final String login,
			@RequestBody @Nullable final UserDTO userDTO
	) {
		if (userDTO == null) return null;
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return null;
		user.setLogin(userDTO.getLogin());
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPasswordNew()));
		return userService.merge(user).transformToDTO();
	}

	@PutMapping(
			value = "/users/admin",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO editUserAdmin(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@Nullable final User user = userService.findOne(userDTO.getId());
		if (user == null) return null;
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPasswordNew()));
		user.getRoles().clear();
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		return userService.merge(user).transformToDTO();
	}

	@DeleteMapping("/users")
	public void removeUser(@RequestBody @NotNull final String login) {
		@Nullable final User user = userService.findByLogin(login);
		if (user == null) return;
		userService.remove(user.getId());
	}

	@DeleteMapping("/users/{id}")
	public void removeUserAdmin(@PathVariable("id") @NotNull final String id) {
		userService.remove(id);
	}

	@GetMapping("/users/user/{userName}")
	public UserDTO getUserByLogin(@PathVariable("userName") @NotNull final String userName) {
		@Nullable final User found = userService.findByLogin(userName);
		return (found == null) ? null : found.transformToDTO();
	}

}
