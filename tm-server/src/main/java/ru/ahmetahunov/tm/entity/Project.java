package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import ru.ahmetahunov.tm.enumerated.Status;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate = new Date(0);

    @NotNull
    @Column(name = "finish_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate = new Date(0);

    @NotNull
    @Column(name = "creation_date")
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    public ProjectDTO transformToDTO() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(this.id);
        project.setName(this.name);
        project.setDescription(this.description);
        project.setStartDate(this.startDate);
        project.setFinishDate(this.finishDate);
        project.setCreationDate(this.creationDate);
        project.setStatus(this.status);
        project.setUserId(this.user.getId());
        return project;
    }

}
