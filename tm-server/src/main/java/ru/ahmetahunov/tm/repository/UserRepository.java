package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.tm.entity.User;

public interface UserRepository extends JpaRepository<User, String> {

	@Nullable
	public User findUserByLogin(@NotNull final String login);

}
