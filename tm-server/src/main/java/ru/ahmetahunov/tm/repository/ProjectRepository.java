package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {

	@Nullable
	public Project findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

	@NotNull
	public List<Project> findAllByUserId(@NotNull final String userId);

	public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
